const {successResponse} = require("../helper/response.js")
const fs = require ('fs')
const {stringify} = require ('querystring')

class User {
    constructor(){
        this.user = []
        const readregist = fs.readFileSync('./data/registered.json', 'utf-8')
        if (readregist == ""){
            this.parsedUser = this.user;
        }else {
            this.parsedUser = JSON.parse(readregist)
        }
    }

    getUser = (req, res) => {
        successResponse (
            res,
            200,
            this.parsedUser,
            {total: this.parsedUser.length}
        )
    }

    getDetailUser= (req, res) => {
        const index = req.params.index
        successResponse (
            res,
            200,
            this.parsedUser[index]
            
        )
    }

    insertUser= (req, res) => {
        const body = req.body

        const param = {
            'username' : body.username,
            'email': body.email

        }
        this.parsedUser.push(param)
        fs.writeFileSync('./data/registered.json', JSON.stringify(this.parsedUser))
        successResponse (res,201,param)
    }

    updateUserinfo = (req, res) => {
        const index = req.params.index
        const body = req.body

        if (this.parsedUser[index]){
            this.parsedUser[index].username = body.username
            this.parsedUser[index].email = body.email
            fs.writeFileSync('./data/registered.json', JSON.stringify(this.parsedUser))
            successResponse (res,200,this.parsedUser[index])
        } else {
            successResponse(res, 422, null)}
    }


    deleteUserinfo= (req, res) => {
        const index = req.params.index
        this.parsedUser.splice(index, 1)
        successResponse (res,200,null)
    }

}
module.exports = User