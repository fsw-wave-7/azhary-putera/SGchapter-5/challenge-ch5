class Card {
    constructor(cardId){
        this.cardId= cardId
        this.option = [
            {
                name: "rock",
                src: 'assets/img/batu.png'
            },
            {
                name: "paper",
                src : 'assets/img/kertas.png'
            },
            {
                name: "scissors",
                src : 'assets/img/gunting.png'
            }

        ]
    
    }

    getCard(){
        const t  = this
        return this.option.forEach((opt) => {
            console.log(opt)
            const img = document.createElement('img')
            img.src = opt.src
            img.className ='m-3'
            img.id= `card-${opt.name}`
            document.getElementById(this.cardId).append(img)
        })
    }

    getOptions(){
        return this.option
    }

    

}

class RandomCard extends Card {
    constructor (cardId){
        super(cardId)
    }

    randomCard(){
        let index = Math.floor(Math.random()* Math.floor(3))
        const  option = super.getOptions()
        return option[index]
    }
}


const card = new Card('card')
card.getCard()
const cardcom = new RandomCard('cardcomp')
cardcom.getCard()


card.getOptions().forEach((opt) => {
    document.getElementById(`card-${opt.name}`).addEventListener('click',() =>{
        document.getElementById(`card-${opt.name}`).style.background = "#C4C4C4";//Menambahkan background pada  pilihan player
        const player = opt
        const  comp = cardcom.randomCard()
        console.log(player.name, comp.name)
        

        let hasil = ''
        if (player.name == comp.name){
            hasil = "DRAW"
        } else if(player.name == 'rock' & comp.name == 'scissors') {
            hasil = 'PLAYER 1 WIN' 
        }else if(player.name == 'rock' & comp.name == 'paper') {
            hasil = 'COM WIN' 
        }else if(player.name == 'scissors' & comp.name == 'paper') {
            hasil = 'PLAYER 1 WIN' 
        }else if(player.name == 'scissors' & comp.name == 'rock') {
            hasil = 'COM WIN' 
        }else if(player.name == 'paper' & comp.name == 'scissors') {
            hasil = 'COM WIN' 
        }else {
            hasil = 'PLAYER 1 WIN' 
        }

        document.getElementById('winprint').innerHTML = hasil
        const winner = document.getElementsByClassName('textbox')[0]
        winner.style.backgroundColor = '#4C9654'
        winner.style.color = 'white'
    
        
    })
})

const reloadtButton = document.querySelector("#reload");
// Reload everything:
function reload() {
    reload = location.reload();
}

