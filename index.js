const express = require('express')
const app = express()
const port = 3000
const User = require('./controller/regist.js')
const bodyParser = require('body-parser')
const jsonParser = bodyParser.json()
const path = require('path')

app.set('view engine', 'ejs')
app.use(express.static(__dirname + '/public'));
app.use(jsonParser)
const user = new User

//memasukkan challenge chapter 3 dan 4 ke server 
app.get('/', function(req, res){
    res.render(path.join(__dirname, './views/index'))
})

app.get('/trial', function(req, res){
    res.render(path.join(__dirname, './views/pages/trial'))
})




//membuat data user static yang akan disimpan ke bentuk json di folder data
app.get('/register', user.getUser)
app.get('/register/:index', user.getDetailUser)
app.post('/register', user.insertUser)
app.put('/register/:index', user.updateUserinfo)
app.delete('/register/:index', user.deleteUserinfo)

app.listen(port, () => {console.log("Server berhasil dijalankan")})